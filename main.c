#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct Racun
{
    int broj;
    char primalac [30];
    char uplatilac [30];
    double iznos;
    bool indikator;
    struct Racun *next;
}Racun;

Racun * dodajNovi (Racun * lst, int broj, char primalac[30], char uplatilac[30], double iznos, bool indikator);
bool plati (Racun * lst, int broj);
Racun * brisi (Racun *lst, int broj);
void prikazi (Racun *lst, char primalac [30]);
void prikaziNeplacene (Racun *lst);
void prikaziSve (Racun *lst);
void stampajRacun (Racun * racun);
Racun * brisiSve (Racun *lst);
Racun * popuniTest(Racun * lst);

int main()
{
    int broj;
    char prim [30], uplat [30];
    double iznos;
    int opcija;
    Racun *lst=0;


    lst = popuniTest(lst);

    while (true)
    {
        printf("\n\nMENI\n");
        printf("1.Dodaj novi racun \n");
        printf("2.Plati postojeci racun \n");
        printf("3.Brisi odredjeni racun preko broja racuna\n");
        printf("4.Prikazi podatke racuna preko primaoca\n");
        printf("5.Prikaz racuna koji nisu placeni\n");
        printf("6.Prikaz svih racuna u bazi\n");
        printf("7.Brisanje svih racuna u bazi\n");
        printf("8.Izlaz!\n");
        scanf("%d",&opcija);
        switch (opcija)
        {
            case 1:
                {
                    printf("Unesite broj racuna: \n");
                    scanf("%d",&broj);
                    printf("Unesite ime primaoca: \n");
                    fflush(stdin);
                    scanf("%[^\n]",prim);
                    printf("Unesite ime uplatioca: \n");
                    fflush(stdin);
                    scanf("%[^\n]",uplat);
                    printf("Unesite iznos: \n");
                    scanf("%lf",&iznos);
                    lst=dodajNovi(lst, broj, prim, uplat, iznos, false);
                    break;

                }
            case 2:
                {
                    printf("Unesite broj racuna koji zelite da platite: \n");
                    scanf("%d",&broj);
                    if(plati(lst,broj))
                        printf("Racun broj %d je placen\n", broj);
                    else
                        printf("Racun broj %d ne postoji\n", broj);
                    break;
                }
            case 3:
                {
                    printf("Unesite broj racuna koji zelite da briste: \n");
                    scanf("%d",&broj);
                    lst=brisi(lst,broj);
                    break;
                }
            case 4:
                {
                    printf("Unesite ime primaoca: \n");
                    fflush(stdin);
                    scanf("%[^\n]",prim);
                    prikazi(lst,prim);
                    break;
                }
            case 5:
                {
                    prikaziNeplacene(lst);
                    break;
                }
            case 6:
                {
                    prikaziSve(lst);
                    break;
                }
            case 7:
                {
                    lst = brisiSve(lst);
                    printf("Svi racuni su obrisani! \n");
                    lst = popuniTest(lst);
                    break;
                }
            case 8:
                {
                    printf("Hvala na koriscenju! Prijatno!\n");
                    exit(EXIT_SUCCESS);
                }
            default:
                {
                    printf("Unesite opciju od 1 do 8!\n");
                }
        }
    }
    return 0;
}

Racun * dodajNovi (Racun * lst, int broj, char primalac[30], char uplatilac[30], double iznos, bool indikator)
{
    Racun * novi = malloc(sizeof(Racun));
    novi->broj = broj;
    strcpy(novi->primalac, primalac);
    strcpy(novi->uplatilac, uplatilac);
    novi->iznos = iznos;
    novi->indikator = indikator;
    novi->next = 0;

    if (lst==NULL)
    {
        lst=novi;
    }
    else
    {
        Racun * trenutni;
        for (trenutni=lst;trenutni->next;trenutni=trenutni->next);
        trenutni->next=novi;
    }
    return lst;
}

bool plati (Racun * lst, int broj)
{
    for (Racun *tekuci=lst;tekuci;tekuci=tekuci->next)
    {
        if (tekuci->broj==broj)
        {
            tekuci->indikator=true;
            return true;
        }
    }
    return false;
}

Racun * brisi (Racun *lst, int broj)
{
    Racun *tekuci,*prethodni,*stari;
    tekuci=lst;
    prethodni=0;
    stari=0;
    while (tekuci)
    {
        if (tekuci->broj!=broj)
        {
            prethodni=tekuci;
            tekuci=tekuci->next;
        }
        else
        {
            stari=tekuci;
            tekuci=tekuci->next;
            if (prethodni==NULL)
            {
                lst=tekuci;
            }
            else
            {
                prethodni->next=tekuci;
            }
            free(stari);
        }
    }
    return lst;
}

void prikazi (Racun *lst, char primalac [30])
{
    for (Racun *trenutni=lst;trenutni;trenutni=trenutni->next)
    {
        if (strcmp(primalac,trenutni->primalac)==0)
            stampajRacun(trenutni);
    }
}
void prikaziNeplacene (Racun *lst)
{
    printf("\n\nSVI NEPLACENI RACUNI:\n\n");
    for (Racun *trenutni=lst;trenutni;trenutni=trenutni->next)
    {
        if (trenutni->indikator==false)
            stampajRacun(trenutni);
    }
}

void prikaziSve (Racun *lst)
{
    printf("\n\nSVI RACUNI:\n\n");
    for (Racun *trenutni=lst;trenutni;trenutni=trenutni->next)
    {
        stampajRacun(trenutni);
    }
}

void stampajRacun (Racun * racun)
{
    printf("\n\nBroj racuna je %d.\nIme primaoca je: %s.\nIme uplatioca je %s.\nIznos je %lf.\nRacun je: %s\n",
           racun->broj, racun->primalac, racun ->uplatilac, racun->iznos, racun->indikator ? "Placen" : "Neplacen");
}

Racun * brisiSve (Racun *lst)
{
    Racun * stari;
    while (lst)
    {
        stari=lst;
        lst=lst->next;
        free(stari);
    }
    return 0;
}

Racun * popuniTest(Racun * lst)
{
    char ch;
    printf("Ukoliko zelite da popunite bazu test podacima unesite slovo d u suprotnom unesite bilo sta drugo\n");
    fflush(stdin);
    scanf("%c", &ch);

    if(ch == 'd' || ch == 'D')
    {
        lst = dodajNovi(lst, 1, "mina", "filip", 1000, false);
        lst = dodajNovi(lst, 2, "filip", "mina", 1000, true);
        lst = dodajNovi(lst, 3, "milan", "mina", 1000, false);
        lst = dodajNovi(lst, 4, "milan", "filip", 1000, false);
        lst = dodajNovi(lst, 5, "filip", "milan", 1000, false);
    }
    fflush(stdin);

    return lst;
}
